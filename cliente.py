class Cliente:
    def __init__(self, nome):
        self.__nome = nome

    @property
    def nome(self):
        #pirint("chamando @property nome()")
        return self.__nome.title()

    @nome.setter
    def nome(self, nome):
        #print("chamando setter nome")
        self.__nome = nome

# no exemplo acima foi demonstrado como fazer como um metodo get e setter se comporte como um atributo ou seja, e totalmente possivel usar os dois metodos 
# nome = cliente.nome
# cliente.nome = "fulano de tal"